# Shinobi Video Browser API

Module to provide Video Browser data for end consumer UI applications

- Module is using both Videos and Time lapse images to build the wall
- If time lapse image was not found for the timeframe of the video, filename will return as null
- For best results, set the time lapse interval to 1 minute, deletion of files will be handled automatically

## How to install

Get latest release from GitLab:

- Open `shinobi-video-browser-api` [releases](https://gitlab.com/elad.bar/shinobi-video-browser-api/-/releases)
- Copy the link of `Source code (zip)` asset of latest version

Install Shinobi customAutoLoad:

- Login to the super dashboard
- Open tab `Custom Auto Load`
- Paste the release asset ZIP file url in the `Download URL for Module`
- Click on `Download`
- In the package added to the list, click on `Enable`
- Restart Shinobi Video

*If previous version of the module is installed, remove it before updateing*

## How to use

### Endpoints

#### Get Video list of monitors

```bash
curl http://url/:auth/videoBrowser/:ke
```

```json
{
    "ok": true,
    "data": [
        {
            "mid": "MonitorID",
            "ke": ":ke"
        }
    ]
}
```

#### Get list of dates available with videos for specific monitor

If time lapse image is available at the same time the video was created, filename property will be available

```bash
curl http://url/:auth/videoBrowser/:ke/:id
```

```json
{
    "ok": true,
    "data": [
        {
            "mid": ":id",
            "ke": ":ke",
            "date": "2022-11-22",
            "filename": "2022-11-22T00-00-06.jpg"
        }
    ]
}
```

#### Get list of videos for specific monitor on specific date (ISO format YYYY-mm-dd)

If time lapse image is available between the start and end time of the video, filename property will be available

```bash
curl http://url/:auth/videoBrowser/:ke/:id/:date
```

```json
{
    "ok": true,
    "data": [
        {
            "mid": "id",
            "ke": ":ke",
            "ext": "mp4",
            "time": "2022-11-22T23:45:00",
            "filename": "2022-11-22T23-45-23.jpg"
        },
    ]
}
```
